package com.kazakimaru.ch05t2.adapter

import android.graphics.drawable.Drawable
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kazakimaru.ch05t2.R
import com.kazakimaru.ch05t2.databinding.ItemCarBinding
import com.kazakimaru.ch05t2.modal.GetAllCarResponseItem
import com.squareup.picasso.Picasso

class CarAdapter : RecyclerView.Adapter<CarAdapter.CarViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<GetAllCarResponseItem>() {
        override fun areItemsTheSame(oldItem: GetAllCarResponseItem, newItem: GetAllCarResponseItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: GetAllCarResponseItem, newItem: GetAllCarResponseItem): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)

    fun updateData(cars: List<GetAllCarResponseItem>) = listDiffer.submitList(cars)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val binding = ItemCarBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CarViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    /**
     * view holder wajib extend RecyclerView ViewHolder
     * ViewHolder butuh view maka kita tambahkan parameter view
     */
    inner class CarViewHolder(private val binding: ItemCarBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: GetAllCarResponseItem) {
            binding.apply {
                tvCarName.text = item.name
                Picasso.get().load(item.image).into(ivCar)
            }
        }
//        fun bind(item: GetAllCarResponseItem) {
//            binding.apply {
//                tvCarName.text = item.name
//                val bgOptions = RequestOptions()
//                    .placeholder(R.drawable.ic_add)
//                Glide.with(itemView.context)
//                    .load(R.drawable.car)
//                    .apply(bgOptions)
//                    .centerCrop()
//                    .into(object : CustomTarget<Drawable?>() {
//                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
//                            ivCar.setImageDrawable(resource)
//                        }
//
//                        override fun onLoadCleared(placeholder: Drawable?) {
//
//                        }
//                    })
//            }
//        }
    }


}