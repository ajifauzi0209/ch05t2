package com.kazakimaru.ch05t2.fragment

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.kazakimaru.ch05t2.R
import com.kazakimaru.ch05t2.adapter.CarAdapter
import com.kazakimaru.ch05t2.databinding.FragmentCarBinding
import com.kazakimaru.ch05t2.databinding.LayoutDialogAddBinding
import com.kazakimaru.ch05t2.modal.GetAllCarResponseItem
import com.kazakimaru.ch05t2.modal.RegisterRequest
import com.kazakimaru.ch05t2.modal.RegisterResponse
import com.kazakimaru.ch05t2.service.ApiClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CarFragment : Fragment() {
    private var _binding: FragmentCarBinding? = null
    private val binding get() = _binding!!

    private lateinit var carAdapter: CarAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        getDataFromNetwork()
        onAddClicked()
    }

    private fun initRecyclerView() {
        carAdapter = CarAdapter()
        binding.rvCar.apply {
            adapter = carAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun getDataFromNetwork() {
        val apiService = ApiClient.instance
        apiService.getAllCar().enqueue(object : Callback<List<GetAllCarResponseItem>> {
            override fun onResponse(call: Call<List<GetAllCarResponseItem>>, response: Response<List<GetAllCarResponseItem>>) {
                if (response.isSuccessful) {
                    if (!response.body().isNullOrEmpty()) {
                        response.body()?.let {carAdapter.updateData(it)}
                    }
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<List<GetAllCarResponseItem>>, t: Throwable) {
                binding.pbCar.isVisible = false
            }

        })
    }

    private fun onAddClicked() {
        binding.fabAdd.setOnClickListener {
            createCustomDialog { email, password ->
                binding.pbCar.isVisible = true
                registerNewAdmin(email, password)
            }
        }
    }

    private fun createCustomDialog(clickListener: (email: String, password: String) -> Unit) {
        val binding = LayoutDialogAddBinding.inflate(LayoutInflater.from(requireContext()), null, false)
        val customLayout = binding.root

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        binding.apply {
            btnRegister.setOnClickListener {
                clickListener.invoke(editEmail.text.toString(), editPassword.text.toString())
                dialog.dismiss()
            }
        }

        dialog.show()
    }

    private fun registerNewAdmin(email: String, password: String) {
        val apiService = ApiClient.instance
        val request = RegisterRequest(email, password, role = "admin")
        apiService.registerAdmin(request).enqueue(object : Callback<RegisterResponse> {
            override fun onResponse(call: Call<RegisterResponse>, response: Response<RegisterResponse>) {
                if (response.isSuccessful) {
                    Toast.makeText(requireContext(), "Register Berhasil", Toast.LENGTH_SHORT).show()
                } else {
                    val objError = JSONObject(response.errorBody()!!.string())
                    val message = objError.getString("message")
                    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
                }
                binding.pbCar.isVisible = false
            }

            override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                binding.pbCar.isVisible = false
            }

        })
    }
}