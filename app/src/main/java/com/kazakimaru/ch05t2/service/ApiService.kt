package com.kazakimaru.ch05t2.service

import com.kazakimaru.ch05t2.modal.GetAllCarResponseItem
import com.kazakimaru.ch05t2.modal.RegisterRequest
import com.kazakimaru.ch05t2.modal.RegisterResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    // Untuk mendapatkan list semua car
    @GET("/admin/car")
    fun getAllCar(): Call<List<GetAllCarResponseItem>>

    @POST("/admin/auth/register")
    fun registerAdmin(@Body registerRequest: RegisterRequest): Call<RegisterResponse>
}